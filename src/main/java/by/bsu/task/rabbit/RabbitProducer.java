package by.bsu.task.rabbit;


import org.apache.commons.lang.SerializationUtils;

import java.io.IOException;
import java.io.Serializable;

public class RabbitProducer extends RabbitConnection {

    public RabbitProducer(String endPointName) throws IOException {
        super(endPointName);
    }

    public void sendMessage(Serializable object) throws IOException {
        channel.basicPublish("",endPointName, null, SerializationUtils.serialize(object));
    }
}
