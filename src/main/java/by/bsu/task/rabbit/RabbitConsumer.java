package by.bsu.task.rabbit;


import by.bsu.task.azure.AzureFunction;
import com.rabbitmq.client.*;
import org.apache.commons.lang.SerializationUtils;

import java.io.IOException;

public class RabbitConsumer extends RabbitConnection implements Runnable, Consumer {

    public RabbitConsumer(String endPointName) throws IOException {
        super(endPointName);
    }

    public void run() {
        try {
            channel.basicConsume(endPointName, true,this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void handleConsumeOk(String consumerTag) {
        System.out.println("Consumer " + consumerTag + " registered");
    }


    public void handleDelivery(String consumerTag, Envelope env,
                               AMQP.BasicProperties props, byte[] body) throws IOException {
        String message = (String) SerializationUtils.deserialize(body);

        System.out.println(AzureFunction.callToAzureFunctions(message));
    }

    public void handleCancel(String consumerTag) {}
    public void handleCancelOk(String consumerTag) {}
    public void handleRecoverOk(String consumerTag) {}

    public void handleShutdownSignal(String consumerTag, ShutdownSignalException arg1) {}
}