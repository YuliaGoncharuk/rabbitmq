package by.bsu.task.rabbit;


import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;

public class RabbitConnection {
    protected Channel channel;
    protected com.rabbitmq.client.Connection connection;
    protected String endPointName;

    public RabbitConnection(String endPointName) throws IOException {
        this.endPointName = endPointName;
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        connection = factory.newConnection();
        channel = connection.createChannel();
        channel.queueDeclare(endPointName, false, false, false, null);
    }
}
