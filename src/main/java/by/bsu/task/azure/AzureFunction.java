package by.bsu.task.azure;


import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.IOException;

public class AzureFunction {

    public static String callToAzureFunctions(String message) throws IOException {
        JSONObject json = new JSONObject();
        json.put("password", message);

        HttpClient client = HttpClients.createDefault();
        HttpPost httpPost =
                new HttpPost("https://passwordvalidation.azurewebsites.net/api/validation?code=JTPTsn/PjluNfeCJQp/TvHCasiwDPEsRhlxIvgSsRF86FqrHAs82Bg==");
        StringEntity entity = new StringEntity(json.toString(), "UTF-8");
        entity.setContentType("application/json");
        httpPost.setEntity(entity);
        HttpResponse response = client.execute(httpPost);

        String validated = EntityUtils.toString(response.getEntity());
        String validationResult = Boolean.parseBoolean(validated)? "valid" : "not valid";

        return "Password " + message + " is: " + validationResult;
    }
}
