package by.bsu.task.runner;


import by.bsu.task.quartz.SchedulerConfig;
import by.bsu.task.rabbit.RabbitConsumer;


public class Runner {

    public static void main(String[] args) throws Exception{
        RabbitConsumer consumer = new RabbitConsumer("queueName");
        Thread consumerThread = new Thread(consumer);
        consumerThread.start();
        SchedulerConfig.getScheduler().start();
    }

}
