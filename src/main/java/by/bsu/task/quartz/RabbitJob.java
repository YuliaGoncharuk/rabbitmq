package by.bsu.task.quartz;


import by.bsu.task.rabbit.RabbitProducer;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.io.IOException;

public class RabbitJob implements Job {
    public void execute(JobExecutionContext context)
            throws JobExecutionException {
        try {
            RabbitProducer producer = new RabbitProducer("queueName");
            String message = "Aspire7551g";
            producer.sendMessage(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
