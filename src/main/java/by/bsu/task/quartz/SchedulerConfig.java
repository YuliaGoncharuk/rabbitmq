package by.bsu.task.quartz;


import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

public class SchedulerConfig {

    public static Scheduler getScheduler() throws SchedulerException {
        Scheduler scheduler = new StdSchedulerFactory().getScheduler();

        JobDetail job = JobBuilder.newJob(RabbitJob.class)
                .withIdentity("myJob", "jobGroup")
                .build();

        Trigger trigger = TriggerBuilder
                .newTrigger()
                .withIdentity("cronTrigger", "triggerGroup")
                .withSchedule(
                        CronScheduleBuilder.cronSchedule("0/5 * * * * ?"))
                .build();

        scheduler.scheduleJob(job, trigger);

        return scheduler;


    }
}
